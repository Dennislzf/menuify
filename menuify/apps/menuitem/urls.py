from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
	url(r'^menudashboard', 'menuitem.views.menu_dashboard'),
	url(r'^addmenuitems', 'menuitem.views.add_menu_items'),

    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
