from django.db import models
from django.contrib.auth.models import User
from sections.models import Section

class MenuItem(models.Model):
	user = models.ForeignKey(User)
	section = models.ForeignKey(Section)
	name = models.CharField(max_length=100)
	order = models.PositiveIntegerField()
	price = models.DecimalField(max_digits = 5,decimal_places=2)
	description = models.CharField(max_length=300)
	ishidden = models.BooleanField(default = False)
	iscomplete = models.BooleanField(default=False)
	ingredients  = models.ManyToManyField('menuitem.Ingredient',related_name="menuitem_ingredient")
	menuitemviews = models.ManyToManyField('menuitem.MenuItemViews',related_name="menuitem_menuitemviews")
	dietaryrestrictions = models.ManyToManyField('menuitem.DietaryRestrictions', related_name="menuitem_dietaryrestrictions")		
	menuitemimages = models.ManyToManyField('menuitem.MenuItemImages', related_name="menuitem_menuitemimages")
	extrainformation = models.ManyToManyField('menuitem.MenuItemImages',related_name = "menuitem_extrainformation")


class Ingredient(models.Model):
	name = models.CharField(max_length=50)
class MenuItemViews(models.Model):
	date = models.DateTimeField(auto_now=True, auto_now_add=True), 

class DietaryRestrictions(models.Model):
	restriction = models.CharField(max_length=50)

class MenuItemImages(models.Model):
	user = models.ForeignKey(User)
	isassigned = models.BooleanField(default = False)
	imageurl = models.CharField(max_length=200)
	imagekey = models.CharField(max_length=200)

class ExtraInformation(models.Model):
	title = models.CharField(max_length=100)
	information  = models.CharField(max_length=200)

