# Create your views here.
from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404  
from userinfo.models import  *
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
import Image
from boto.s3.key import Key
from menustyle.models import *
from sections.models import *
from photouploader.views import *
from menuitem.models import *

def menu_dashboard(request):
	#check to see if user is logged in
	if request.user.is_authenticated()== False:
		return HttpResponseRedirect('/myaccount/login')
	#get all the menu styles
	try:
		sections  = Section.objects.filter(user = request.user)
	except:
		raise Http404
	#if there are bannerimages, then get the banner images
		
	#pass form args
	form_args = {'sections':sections}
	if request.method == "GET":
		return render_to_response("menuitem/menu_dashboard.html",form_args,context_instance=RequestContext(request))
	
def add_menu_items(request):
	if request.user.is_authenticated() == False:
		return HttpResponseRedirect('/myaccount/login')

	#get itemimages that are not assigned
	if request.method == "GET":
		#get all the unassigned images
		unassigneditemimages = MenuItemImages.objects.filter(user=request.user,isassigned=False)
		form_args={'unassigneditemimages':unassigneditemimages}
		return render_to_response("menuitem/add_menu_items.html",form_args,context_instance=RequestContext(request))
	



		
