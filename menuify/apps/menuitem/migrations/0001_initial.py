# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MenuItem'
        db.create_table('menuitem_menuitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sections.Section'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('ishidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('iscomplete', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('menuitem', ['MenuItem'])

        # Adding M2M table for field ingredients on 'MenuItem'
        m2m_table_name = db.shorten_name('menuitem_menuitem_ingredients')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuitem', models.ForeignKey(orm['menuitem.menuitem'], null=False)),
            ('ingredient', models.ForeignKey(orm['menuitem.ingredient'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuitem_id', 'ingredient_id'])

        # Adding M2M table for field menuitemviews on 'MenuItem'
        m2m_table_name = db.shorten_name('menuitem_menuitem_menuitemviews')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuitem', models.ForeignKey(orm['menuitem.menuitem'], null=False)),
            ('menuitemviews', models.ForeignKey(orm['menuitem.menuitemviews'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuitem_id', 'menuitemviews_id'])

        # Adding M2M table for field dietaryrestrictions on 'MenuItem'
        m2m_table_name = db.shorten_name('menuitem_menuitem_dietaryrestrictions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuitem', models.ForeignKey(orm['menuitem.menuitem'], null=False)),
            ('dietaryrestrictions', models.ForeignKey(orm['menuitem.dietaryrestrictions'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuitem_id', 'dietaryrestrictions_id'])

        # Adding M2M table for field menuitemimages on 'MenuItem'
        m2m_table_name = db.shorten_name('menuitem_menuitem_menuitemimages')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuitem', models.ForeignKey(orm['menuitem.menuitem'], null=False)),
            ('menuitemimages', models.ForeignKey(orm['menuitem.menuitemimages'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuitem_id', 'menuitemimages_id'])

        # Adding M2M table for field extrainformation on 'MenuItem'
        m2m_table_name = db.shorten_name('menuitem_menuitem_extrainformation')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuitem', models.ForeignKey(orm['menuitem.menuitem'], null=False)),
            ('menuitemimages', models.ForeignKey(orm['menuitem.menuitemimages'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuitem_id', 'menuitemimages_id'])

        # Adding model 'Ingredient'
        db.create_table('menuitem_ingredient', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('menuitem', ['Ingredient'])

        # Adding model 'MenuItemViews'
        db.create_table('menuitem_menuitemviews', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('menuitem', ['MenuItemViews'])

        # Adding model 'DietaryRestrictions'
        db.create_table('menuitem_dietaryrestrictions', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('restriction', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('menuitem', ['DietaryRestrictions'])

        # Adding model 'MenuItemImages'
        db.create_table('menuitem_menuitemimages', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('isassigned', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('imageurl', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('imagekey', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('menuitem', ['MenuItemImages'])

        # Adding model 'ExtraInformation'
        db.create_table('menuitem_extrainformation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('information', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('menuitem', ['ExtraInformation'])


    def backwards(self, orm):
        # Deleting model 'MenuItem'
        db.delete_table('menuitem_menuitem')

        # Removing M2M table for field ingredients on 'MenuItem'
        db.delete_table(db.shorten_name('menuitem_menuitem_ingredients'))

        # Removing M2M table for field menuitemviews on 'MenuItem'
        db.delete_table(db.shorten_name('menuitem_menuitem_menuitemviews'))

        # Removing M2M table for field dietaryrestrictions on 'MenuItem'
        db.delete_table(db.shorten_name('menuitem_menuitem_dietaryrestrictions'))

        # Removing M2M table for field menuitemimages on 'MenuItem'
        db.delete_table(db.shorten_name('menuitem_menuitem_menuitemimages'))

        # Removing M2M table for field extrainformation on 'MenuItem'
        db.delete_table(db.shorten_name('menuitem_menuitem_extrainformation'))

        # Deleting model 'Ingredient'
        db.delete_table('menuitem_ingredient')

        # Deleting model 'MenuItemViews'
        db.delete_table('menuitem_menuitemviews')

        # Deleting model 'DietaryRestrictions'
        db.delete_table('menuitem_dietaryrestrictions')

        # Deleting model 'MenuItemImages'
        db.delete_table('menuitem_menuitemimages')

        # Deleting model 'ExtraInformation'
        db.delete_table('menuitem_extrainformation')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'menuitem.dietaryrestrictions': {
            'Meta': {'object_name': 'DietaryRestrictions'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'restriction': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menuitem.extrainformation': {
            'Meta': {'object_name': 'ExtraInformation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'information': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'menuitem.ingredient': {
            'Meta': {'object_name': 'Ingredient'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menuitem.menuitem': {
            'Meta': {'object_name': 'MenuItem'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'dietaryrestrictions': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'menuitem_dietaryrestrictions'", 'symmetrical': 'False', 'to': "orm['menuitem.DietaryRestrictions']"}),
            'extrainformation': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'menuitem_extrainformation'", 'symmetrical': 'False', 'to': "orm['menuitem.MenuItemImages']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredients': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'menuitem_ingredient'", 'symmetrical': 'False', 'to': "orm['menuitem.Ingredient']"}),
            'iscomplete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ishidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'menuitemimages': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'menuitem_menuitemimages'", 'symmetrical': 'False', 'to': "orm['menuitem.MenuItemImages']"}),
            'menuitemviews': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'menuitem_menuitemviews'", 'symmetrical': 'False', 'to': "orm['menuitem.MenuItemViews']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sections.Section']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'menuitem.menuitemimages': {
            'Meta': {'object_name': 'MenuItemImages'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagekey': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'imageurl': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'isassigned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'menuitem.menuitemviews': {
            'Meta': {'object_name': 'MenuItemViews'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'sections.section': {
            'Meta': {'object_name': 'Section'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }

    complete_apps = ['menuitem']