from django import forms
from userinfo.models import *
from menustyle.models import *
from sections.models import  *
from django.http import Http404 
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit,Button
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class SignUpForm(forms.Form):
    email = forms.EmailField(
        required = True,
    )

    password = forms.CharField(
     label = "Password",
     widget = forms.PasswordInput()

    )

    confirmpassword = forms.CharField(
      label = "Confirm Password",
      widget = forms.PasswordInput()
    
    )
    companyname = forms.CharField(
        label = "Company Name",
        max_length = 100,
        required = True,
    )
    firstname = forms.CharField(
        label = "First Name",
        max_length = 100,
        required = True,
    )
    lastname = forms.CharField(
        label = "Last Name",
        max_length = 100,
        required = True,
    )
    def __init__(self,*args, **kwargs):
        
        self.helper = FormHelper()
        self.helper.form_id = 'sigupform'
        self.helper.form_class = 'form-horizontal signupform'
        self.helper.form_method = 'post'
        self.helper.form_action = 'signup'

        self.helper.add_input(Submit('signup', 'Signup', css_class="btn-large signupbutton"))
    
        super(SignUpForm, self).__init__(*args, **kwargs)
    #check and make sure that the data is clean 
    def clean(self):
        #check to see if email already exists in the system
	cleaned_data = super(SignUpForm,self).clean()
        email = self.cleaned_data.get('email')

        try:
            user = User.objects.get(username = email)
	    userexists=True
        except:
            userexists = False   
	#check to see if user email alreday exists in system   
	if userexists == True:
            raise forms.ValidationError("That email is already in use. Try another one.")
        #check to see if passwords matched    
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('confirmpassword')
	return cleaned_data

        if password1 and password1 != password2:
            raise forms.ValidationError("Your Passwords do not match")    


    #process data and create new user as well as log user in
    def process(self):
	    cleaned_data = super(SignUpForm,self).clean()
	    email = self.cleaned_data.get('email')
	    password = self.cleaned_data.get('password')
	    companyname = self.cleaned_data.get('companyname')
	    firstname = self.cleaned_data.get('firstname')
	    lastname = self.cleaned_data.get('lastname')
	    #create user  
	    user = User.objects.create_user(email,email, password)
	    user.first_name = firstname
	    user.last_name = lastname
	    user.save()
	    #create userinfo that goes with the user
	    userinfo = UserInfo(companyname = companyname, user = user)
	    userinfo.save()
	    #create a menustyle(each user has one)
	    menustyle = MenuStyle(user = user)
	    menustyle.save()
	    #create default menu sections for user
	    #create appetizer section
	    Section(user=user,name = "Appetizer",order=1).save()
	    #create entree section
	    Section(user=user,name = "Entree", order = 2).save()
	    #create dessert section
	    Section(user = user,name= "Dessert",order = 3).save()
	    #create drink section
	    Section(user=  user, name = "Drinks",order = 4).save()
	    #authenticate user
	    user = authenticate(username = email, password = password)
	    return user


	    


class InformationForm(forms.Form):
	city = forms.CharField(
		label= "City",
		max_length= 50,
		required = False
    )
	provincestate = forms.CharField(
		label= "Province/State",    
		max_length= 50,
		required = False
    )
	country = forms.CharField(
		label= "Country",
		max_length= 50,
		required = False
    )
	postalcode = forms.CharField(
		label= "Postal/Zip Code",
		max_length= 10,
		required = False
    )
	phonenumber = forms.CharField(
		label= "Phone Number",
		max_length= 20,
		required = False
    )



	def __init__(self,*args, **kwargs):
        
		self.helper = FormHelper()
		self.helper.form_id = 'informationform'
		self.helper.form_class = 'form-horizontal signupform'
		self.helper.form_method = 'post'
		self.helper.form_action = 'addinformation'

		self.helper.add_input(Submit('saveandcontinue', 'Save and Continue', css_class="btn-large saveandcontinue "))
		self.helper.add_input(Button('Skip', 'Skip', css_class="btn-large btn-danger "))

		#get user
		self.user = kwargs.pop('user',None)
    
		super(InformationForm, self).__init__(*args, **kwargs)

    #check and make sure that the data is clean 
	def clean(self):
		cleaned_data = super(InformationForm,self).clean()
		return cleaned_data
	def process(self):
		#get all thecleaned data if it exists
		cleaned_data = super(InformationForm,self).clean()
		city  = self.cleaned_data.get('city')
		provincestate  = self.cleaned_data.get('provincestate')
		country  = self.cleaned_data.get('country')
		postalcode  = self.cleaned_data.get('postalcode')
		phonenumber  = self.cleaned_data.get('phonenumber')
		#get user
		user = self.user
		#get userinfo	
		userinfo = UserInfo.objects.get(user=user)
		#update userinfo model with all the information
		userinfo.city = city
		userinfo.provincestate=provincestate
		userinfo.country = country
		userinfo.postalcode = postalcode
		userinfo.phonenumber  = phonenumber
		userinfo.save()

    	
