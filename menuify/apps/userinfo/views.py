from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404  
from userinfo.models import  *
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
import Image
from django.core.validators import validate_email
from django.core.mail import EmailMessage,send_mail
from django.template.loader import render_to_string
from boto.s3.key import Key
from userinfo.forms import *
from sections.models import *






def user_signup(request):
	if request.method == "GET":
		signupform = SignUpForm()
		form_args = {'signupform':signupform}
		return render_to_response("userinfo/user_signup.html",form_args,context_instance=RequestContext(request))
	elif request.method == "POST":
		signupform = SignUpForm(request.POST)
		form_args = {'signupform':signupform}
		if signupform.is_valid():
			#creates new user and logs them in
			user = signupform.process()
			login(request,user)
			return HttpResponseRedirect('/myaccount/addinformation')
		else:
			return render_to_response("userinfo/user_signup.html",form_args,context_instance=RequestContext(request))	
#step 2 of the process
def add_information(request):
	#if user is not logged in
	if request.user.is_authenticated() == 'False':
		raise Http404
	if request.method == "GET":
		informationform = InformationForm()
		form_args = {'informationform' : informationform}
		return render_to_response("userinfo/add_information.html",form_args,context_instance=RequestContext(request))	
	elif request.method == "POST":
		informationform  = InformationForm(request.POST,user= request.user)
		form_args = {'informationform':informationform}
		#check if form is valid
		if informationform.is_valid():
			informationform.process()
			return HttpResponseRedirect("/myaccount/customize")
			
		else:	
			return render_to_response("userinfo/add_information.html",form_args,context_instance=RequestContext(request))	



	
	else:
		raise Http404


def dashboard(request):
	if request.user.is_authenticated() == "False":
		return HttpResponseRedirect("/signup")
	if request.method == "GET":
		#get all the sections that have been created
		menusections = Section.objects.filter(user=request.user).order_by('order')
		form_args = {'menusections':menusections}
		return render_to_response("userinfo/dashboard.html",form_args,context_instance=RequestContext(request))	


