from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.conf import settings



                    

class UserInfo(models.Model):
	user  = models.ForeignKey(User)
	companyname = models.CharField(max_length=300)
	companybannerlogo = models.CharField(max_length=200)
	companydescription = models.CharField(max_length=300)
	profilepictureurl = models.CharField(max_length = 200)
	s3key = models.CharField(max_length=200,blank = True, null = True)
	description = models.CharField(max_length = 500)
	country = models.CharField(max_length=50,blank = False)
	provincestate = models.CharField(max_length =  50, blank=False)
	city = models.CharField(max_length=50,blank=False)
	phonenumber = models.CharField(max_length = 20)
	twitterhandle = models.CharField(max_length=30,blank= False)
	website = models.CharField(max_length=50,blank=False)
	instagramhandle=models.CharField(max_length=30,blank = False)
	itemsposted = models.IntegerField(default = 0)
	pintresthandle= models.CharField(max_length= 30)
	numvisitorstotal = models.IntegerField(default= 0)
    



