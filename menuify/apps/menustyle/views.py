from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import  HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.sites.models import Site
import string
import random
import logging
from django.http import Http404  
from userinfo.models import  *
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
import Image
from boto.s3.key import Key
from menustyle.models import *
from sections.models import *
from photouploader.views import *

def edit_menu_style(request):
	#check to see if user is logged in
	if request.user.is_authenticated()== False:
		return HttpResponseRedirect('/myaccount/login')
	#get all the menu styles
	try:
		menustyle = MenuStyle.objects.get(user = request.user)
	except:
		raise Http404
	#if there are bannerimages, then get the banner images
		
	#pass form args
	form_args = {'menustyle':menustyle}
	if request.method == "GET":
		bannerimages = BannerImage.objects.filter(menustyle = menustyle)
		#add banner images to form_args	
		form_args['bannerimages'] = bannerimages	
		return render_to_response("menustyle/edit_menu_style.html",form_args,context_instance=RequestContext(request))
	elif request.method == "POST":
		imagetype = request.POST.get('uploadtype')
		images= request.FILES.getlist('uploadimages')
		#if it is a banner image, make sure that there is 4 or less
		if imagetype == "bannerimage":
			#get num of banner images already present
			countprev = BannerImage.objects.filter(menustyle=menustyle).count()
			#get num of images in images list
			countimages = len(images)
			if countimages + countprev > 4:
				bannerimages = BannerImage.objects.filter(menustyle=menustyle)
				form_args['bannerimages'] = bannerimages	
				form_args['errormessage'] = "You can't upload more then 4 banner images please delete some if you would like to add another."
				return render_to_response("menustyle/edit_menu_style.html",form_args,context_instance=RequestContext(request))
		#if imagetype is companylogo then delete old one	
		if imagetype == "companylogo":
			#after post, keep companylogo page open
			form_args['companylogo'] =True
			if menustyle.logourl != "":
				deleteImage(menustyle.logokey)
				menustyle.logourl = ""
				menustyle.logokey = ""
				menustyle.save()
			
	

		#upload photo function found in apps/photouploader/views
		#if returns None that means method was successful, else problem occured
		uploadphoto = upload_photos(request,imagetype,images)
		#get banner images
		bannerimages = BannerImage.objects.filter(menustyle = menustyle)
		#if uploadphoto returns a errormessage then add to form args
		if uploadphoto:
			errormessage = None
		else:
			errormessage = uploadphoto
		form_args['bannerimages'] = bannerimages
		form_args['errormessage'] = errormessage
		return render_to_response("menustyle/edit_menu_style.html",form_args,context_instance=RequestContext(request))

		


