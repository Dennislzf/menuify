from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class MenuStyle(models.Model):
	user = models.ForeignKey(User)
	allowbannerimages = models.BooleanField(default=False)
	bannerimages = models.ManyToManyField('menustyle.BannerImage',related_name='menustyle_bannerimages')
	istextlogo = models.BooleanField(default=True)
	ispictureloge= models.BooleanField(default=False)
	logourl = models.CharField(max_length= 200)
	logokey = models.CharField(max_length=200)

class BannerImage(models.Model):
	menustyle= models.ForeignKey(MenuStyle)
	ismainimage=  models.BooleanField(default= False)
	url = models.CharField(max_length= 200)
	key = models.CharField(max_length=200)
