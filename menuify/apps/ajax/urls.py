from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('',
	url(r'^deletebannerimage', 'ajax.views.delete_banner_image'),
	url(r'^loadcompanylogo', 'ajax.views.load_company_logo'),
	url(r'^getmenusections/(?P<tag>\w+)/', 'ajax.views.get_menu_sections'),
	url(r'^loadsectiondata/(?P<sectionname>\w+)/(?P<sectionid>\w+)/', 'ajax.views.load_section_data'),
	url(r'^savenewsection', 'ajax.views.save_new_section'),
	url(r'^uploadimage', 'ajax.views.uploadimage'),

    #set root for static files (css, images, etc)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
) 
