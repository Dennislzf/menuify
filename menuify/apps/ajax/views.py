from django.shortcuts import *
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import authenticate,login,get_user
from django.http import *
from menustyle.models import *
from sections.models import *
from menuitem.models import *
from django.template.loader import render_to_string
from django.core.context_processors import csrf
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import simplejson

def delete_banner_image(request):
	if request.method == "POST": 
		bannerid = request.body
		try:
			#get menustyle
			menustyle = MenuStyle.objects.get(user = request.user)
			#get banner image
			bannerimage = BannerImage.objects.get(menustyle = menustyle, id = bannerid)
			deleteImage(bannerimage.key)
			bannerimage.delete()
		except:
			raise Http404
		return HttpResponse("")
	else:
		raise Http404

def load_company_logo(request):
	#try to get logo

	try:
		menustyle = MenuStyle.objects.get(user = request.user)

	except:	
		raise Http404
	csrf_token_value = request.COOKIES['csrftoken']

	form_args = {'menustyle':menustyle,'MEDIA_URL':settings.MEDIA_URL, 'STATIC_URL':settings.STATIC_URL,'csrf_token_value':csrf_token_value}
	return HttpResponse(render_to_string('menustyle/edit_company_logo.html',form_args))



#get menu sections
def get_menu_sections(request,tag):

	try:
		user = User.objects.get(id = tag)
		sections= Section.objects.filter(user = user).order_by('order').values('id','name','order')
	except:
		raise Http404


	#have to place queryset to return as json
	sections = json.dumps(list(sections), cls=DjangoJSONEncoder)
	return HttpResponse(sections, content_type="application/json")

def load_section_data(request,sectionname,sectionid):
	try:
		section = Section.objects.get(name = sectionname, id= sectionid)
		items = MenuItem.objects.filter(section =section).order_by('order')
	except:
		raise Http404
	#check to see if user owns the items
	if(section.user == request.user):
		isadmin = True
	else:
		isadmin = None

	form_args = {'MEDIA_URL':settings.MEDIA_URL, 'STATIC_URL':settings.STATIC_URL, 'items':items,'isadmin':isadmin}
	return HttpResponse(render_to_string('menuitem/load_menu_section.html',form_args))




#save new section
def save_new_section(request):
	sectionname = request.body
	#check to see if section name already exists
	try:
		section = Section.objects.get(user = request.user, name= sectionname)
		return HttpResponse(status = 500)
	except:
		pass
	try:
		#check to see if section name already exists
		numsections = Section.objects.filter(user= request.user).count() + 1
		section = Section(user =request.user, name = sectionname, order = numsections)
		section.save()
	except:
		raise Http404
	return HttpResponse(status = 200)


def upload_image(request):
	return HttpResponse()
