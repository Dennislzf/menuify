# Create your views here.
from decimal import *
import datetime
from django.conf import settings
from PIL import Image
from boto.s3.key import Key
from menustyle.models import *
import boto
import cStringIO
import imghdr
import urllib
from django.http import Http404



def upload_photos(request,imagetype,images):
	print images
	for image in images:
		try:
	    		testimage= Image.open(image)

		except:
				#if file type is not an image that is supported	
		    	return "The image with filename " + image.name + " is not supported."
			
				
				#if imagefile is supported
		#check to see that image size is no larger then 4mb
		if image.size > 5242880:
			return "The image with filename " + image.name + " is too large (over 4mb)."
		#else is image is not that size

		#connect to s3 bucket
		conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
		bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
		k= Key(bucket)

		
		#get time and format to make eah filename unique
		time = str(datetime.datetime.now())
		time.replace(" ","")
		time.replace(".","")
		#get file name and extention time
		filename = image.name
		#check to see if file type is image	
		if filename.endswith('.jpg') or filename.endswith('.jpeg'):
			fileextention = 'jpeg'
		elif filename.endswith('.png'):
			fileextention = 'png'
		elif filename.endswith('.gif'):
			fileextention = 'gif'
		else:
			fileextention = None
		#get filename to save to
		filename = filename.split('.')
		filename = filename[0]
		if fileextention == None:
			return "The image with filename " + image.name + " is not an image"

		#handle bannerimage specific requiredments	
		if imagetype == "bannerimage":
			maxwidth = 1170
			maxheight = 315
			#saveurl to point there
			url ='photos/bannerimage/'+filename+time+"."+fileextention
			#get key name to upload to s3, filename
			k.key='photos/bannerimage/'+filename+time+"."+fileextention
			#get menustyle to create new banned image
			try:
				menustyle = MenuStyle.objects.get(user = request.user)
			except:
				raise Http404	
			newimage = BannerImage(menustyle = menustyle)
			newimage.url = url	
			newimage.key = k.key
			print newimage
			newimage.save()

		#handle company specific requiredments		
		elif imagetype == 'companylogo'	:
			#scale image
			maxwidth=350 
			maxheight=160
			#saveurl to point there
			url ='photos/logo/'+filename+time+"."+fileextention
			#get key name to upload to s3, filename
			k.key='photos/logo/'+filename+time+"."+fileextention
			try:
				menustyle = MenuStyle.objects.get(user = request.user)
			except:
				raise Http404
			
			menustyle.logourl = url
			menustyle.logokey = k.key
			menustyle.save()
		if int(testimage.size[0]) > maxwidth or int(testimage.size[1])>maxheight:
			#see if width is more is more
			if float(testimage.size[0])/float(testimage.size[1]) >1:
				widthratio=  float(testimage.size[1])/float(testimage.size[0])
				height = int(maxwidth*widthratio)
				size = (maxwidth,height)
				testimage = testimage.resize(size,Image.ANTIALIAS)
			#see if height is more is more
			if float(testimage.size[1])/float(testimage.size[0]) >1:
				heightratio=  float(testimage.size[0])/float(testimage.size[1])
				width = int(heightratio*maxwidth)
				size = (width,maxheight)
				testimage = testimage.resize(size,Image.ANTIALIAS)	

		


		out_im2 = cStringIO.StringIO()
		#save image to s3	
		testimage.save(out_im2, fileextention, quality=50)
		k.set_contents_from_string(out_im2.getvalue())
		#set as public
		k.set_acl('public-read')
		#set url and key in s3 server
		url=urllib.quote_plus(url)	
			#return None to show that nothing went wrong
	return 




def deleteImage(key):
	conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
	bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)	
	k = Key(bucket)	
	k.key = key
	bucket.delete_key(k)

