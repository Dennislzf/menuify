from django import forms
from userinfo.models import *
from menustyle.models import *
from sections.models import  *
from django.http import Http404 
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit,Button
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class PhotoUploadForm(forms.Form):
	photo  = forms.FileField()



	def __init__(self,*args, **kwargs):
        
		self.helper = FormHelper()
		self.helper.form_id = 'informationform'
		self.helper.form_class = 'form-horizontal signupform'
		self.helper.form_method = 'post'
		self.helper.form_action = 'addinformation'

		self.helper.add_input(Submit('saveandcontinue', 'Save and Continue', css_class="btn-large saveandcontinue "))
		self.helper.add_input(Button('Skip', 'Skip', css_class="btn-large btn-danger "))

		#get user
		self.user = kwargs.pop('user',None)
    
		super(InformationForm, self).__init__(*args, **kwargs)

    #check and make sure that the data is clean 
	def clean(self):
		cleaned_data = super(InformationForm,self).clean()
		return cleaned_data
	def process(self):
		#get all thecleaned data if it exists
		cleaned_data = super(InformationForm,self).clean()
		city  = self.cleaned_data.get('city')
		provincestate  = self.cleaned_data.get('provincestate')
		country  = self.cleaned_data.get('country')
		postalcode  = self.cleaned_data.get('postalcode')
		phonenumber  = self.cleaned_data.get('phonenumber')
		#get user
		user = self.user
		#get userinfo	
		userinfo = UserInfo.objects.get(user=user)
		#update userinfo model with all the information
		userinfo.city = city
		userinfo.provincestate=provincestate
		userinfo.country = country
		userinfo.postalcode = postalcode
		userinfo.phonenumber  = phonenumber
		userinfo.save()

 
