menuifyModule = angular.module('Menuify', ['angularFileUpload']);
       // shared service that connects success alertbox
	menuifyModule.factory('sharedSuccessBoxService',function($rootScope){
		var successBoxService={};
		successBoxService.show = false;	
		successBoxService.text="Success"

		successBoxService.setShow = function(val){
			this.show = val
			this.broadcastItem()
		};
		successBoxService.setText  = function(val){
			this.text= val
			this.broadcastItem()
		}

		successBoxService.broadcastItem = function(){
			$rootScope.$broadcast('handleSuccessBoxBroadcast')
		};
		return successBoxService
	});
//get data for sections
menuifyModule.factory('sharedSectionItemsService',function($rootScope,$http){
		var sectionItemsService={};
		sectionItemsService.sectionnameopen = ""
		sectionItemsService.sectionidopen = null
		sectionItemsService.sections = []
		//set new section to be open
		sectionItemsService.setSectionOpen = function(sectionname,sectionid){
			sectionItemsService.sectionnameopen = sectionname
			sectionItemsService.sectionidopen = sectionid
		}
		sectionItemsService.setSections = function(sections){
			sectionItemsService.sections = sections
		}
		sectionItemsService.broadcastSectionChange = function(){
			$rootScope.$broadcast('handleSectionsChangeBroadcast')
		}
		sectionItemsService.broadcastSectionsLoaded = function(){
			$rootScope.$broadcast('handleSectionsLoadedBroadcast')
		};
		return sectionItemsService
});
menuifyModule.config(function($httpProvider){
    $httpProvider.defaults.headers.common['X-CSRFToken'] = CSRF_TOKEN;
});

//change symbol "{{" to "{[{" for Angular
menuifyModule.config(function($interpolateProvider) {
	  $interpolateProvider.startSymbol('{[{');
		    $interpolateProvider.endSymbol('}]}');
});



//controller that controls the success message at the top of each page
function SuccessAlertController($scope,successBoxService){
	$scope.show =successBoxService.show
	$scope.successmessage = successBoxService.text 
	//get value of show	
	$scope.getShow = function(){
		return $scope.show;
	};

	$scope.$on('handleSuccessBoxBroadcast',function(){
		$scope.show =successBoxService.show
		$scope.successmessage = successBoxService.text
	});
}
//controller that controls that menu in the edit menu style options
function MenuStyleMenuController($scope,$http){
	$scope.showcompanylogocontainer= false
	$scope.showbannerimagecontainer = true	
	$scope.showpreviewcontainer = false
	isCompanyLogoLoad = false

	//if company logo page has not been loaded yet, then load else return flse
	$scope.loadCompanyLogo = function(){
		if (isCompanyLogoLoad == false){
			$http.get('/ajax/loadcompanylogo/')
				.success(function(data){
					$('.companylogosection').html(data) 

				})
				.error(function(data){

				})
		}	

		
		else{
			return false
		}	
		
	}

	//handle loading containers
	$scope.showMenuItem = function(showitem){
		if(showitem == "bannerimage"){
			$scope.showbannerimagecontainer = true
			$scope.showpreviewcontainer = false;
			$scope.showcompanylogocontainer = false
			$scope.iscompanylogoload = false	
			$("#companylogolist").removeClass('active')
			$("#previewlist").removeClass('active')
			$("#bannerimagelist").addClass('active')
				
		}	
		else if (showitem =="preview"){
			$scope.showbannerimagecontainer = false
			$scope.showpreviewcontainer = true;
			$scope.showcompanylogocontainer = false
			$("#companylogolist").removeClass('active')
			$("#previewlist").addClass('active')
			$("#bannerimagelist").removeClass('active')
			
		}
	}

	}
	


//controller that deals with bannerimages
function BannerImageController($scope, $http,successBoxService){
	
	$scope.deleteBannerImage= function(bannerid){
			  $http.post('/ajax/deletebannerimage/',bannerid)
			    .success(function(data, status, headers, config) {
				    //remove the bannerimage
				    $("#bannerimageholder"+bannerid).remove()
				    //change successmessage and show
				    successBoxService.setText("Your banner image has been successfully deleted")
			    	    successBoxService.setShow(true);		    

			    })
			    .error(function(err) {
				    console.log("Error:" +err)
			      
			    });
	  }

}

//menu of section
function MenuSectionController($scope,$http, successBoxService,sectionItemsService){
 	$scope.sections = {};
	$scope.sectionnameopen= ""
	$scope.sectionidopen = null	
	$scope.addsection = false;	
	$scope.showaddsection= false;	
	$scope.addsectionname = ""
	

	//get all the sections available, ajax load
		$scope.getSections = function(userid){
			$http.get('/ajax/getmenusections/'+userid)
			.success(function(data){
				//set sections
				$scope.sections=  data
				//show the first section that isopen
				$scope.sectionnameopen = data[0].name
				$scope.sectionidopen = data[0].id
				//set sections in shared service
				sectionItemsService.setSections(data)
				//set open section
				sectionItemsService.setSectionOpen($scope.sectionnameopen,$scope.sectionidopen)
				//broadcast that sections have been loaded
				sectionItemsService.broadcastSectionsLoaded()
			})	
		}
	//change section that is being shown
		$scope.showSection = function(sectionname,sectionid){
			//check to see if user clicks same menu section
			if(sectionname != $scope.sectionnameopen){
				sectionItemsService.setSectionOpen(sectionname,sectionid)
				$scope.sectionnameopen = sectionname	
				$scope.sectionidopen = sectionid
				sectionItemsService.broadcastSectionChange()
			}

		}	
			
		//create and save a new section
	$scope.saveNewSection = function(){
		newsection = $scope.addsectionname
		$http.post('/ajax/savenewsection/', newsection)
			.success(function(data){
				$scope.getSections()
				$scope.addsectionname = ""
				$scope.showaddsection =false
				successBoxService.setText("Your Section <strong>"+newsection+ "</strong>has been added")
			    	successBoxService.setShow(true);		    


			})
			.error(function(err){
				//add error for same name for each section 
			})
		

	}
}


function SectionItemsController($scope,$http, successBoxService,sectionItemsService){
 	$scope.sections = {};
	$scope.sectionnameopen=""
	$scope.sectionidopen=null	
	$scope.showaddsection= false;	
	$scope.addsectionname = ""
	//function used to handle broadcast when user changes section
	$scope.$on('handleSectionsChangeBroadcast',function(){
		//hide previous container
		$scope.sectionnameopen = sectionItemsService.sectionnameopen
		$scope.sectionidopen = sectionItemsService.sectionidopen
		$scope.loadSectionData($scope.sectionnameopen,$scope.sectionidopen)
		
	})
	
	//when sections are loaded, then create sections
	$scope.$on('handleSectionsLoadedBroadcast',function(){
		$scope.sections = sectionItemsService.sections
		$scope.sectionnameopen = sectionItemsService.sectionnameopen
		$scope.sectionidopen = sectionItemsService.sectionidopen
		$scope.loadSectionData($scope.sectionnameopen,$scope.sectionidopen)
		
	
	})
	//load items that are the section	
	$scope.loadSectionData  = function(sectionname,sectionid){
		//check to see if data has been loaded
		if($('#'+sectionname+'menuitems').hasClass('loaded')==false )
		{
			$http.get('/ajax/loadsectiondata/'+sectionname+"/"+sectionid)
			.success(function(data){
				$('#'+sectionname+'menuitems').html(data)
				//set class to loaded so data isnt loaded again
				$('#'+sectionname+'menuitems').addClass('loaded')
				

			})
		}
		else{
			return false;
		}
		}

	
}
function AddMenuItemsController($scope,$http,Uploader){
	$scope.menuitems ={}





}

//Controller that allows user to upload pictures using ajax
var AjaxUploadFileController = [ '$scope', '$upload', function($scope, $upload) {
  
  $scope.onFileSelect = function($files,imagestyle) {
    //$files: an array of files selected, each file has name, size, and type.
    for (var i = 0; i < $files.length; i++) {
      var $file = $files[i];
      $scope.upload = $upload.upload({
        url: '/ajax/uploadmenuimage', //upload.php script, node.js route, or servlet url
        // method: POST or PUT,
        // headers: {'headerKey': 'headerValue'}, withCredential: true,
        data: {myObj: $scope.myModelObj, imagestyle:imagestyle},
        file: $file,
         //set file formData name for 'Content-Desposition' header. Default: 'file' 
        //fileFormDataName: myFile,
        /* customize how data is added to formData. See #40#issuecomment-28612000 for example */
        //formDataAppender: function(formData, key, val){} 
      }).progress(function(evt) {
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      }).success(function(data, status, headers, config) {
        // file is uploaded successfully
        console.log(data);
      });
      //.error(...)
      //.then(success, error, progress); 
    }
  };
}];

function MenuDashboardController($scope,$http){
}
function CompanyLogoController($scope,$http){
}

//Injections
BannerImageController.$inject = ['$scope','$http', 'sharedSuccessBoxService'];        
MenuStyleMenuController.$inject = ['$scope','$http'];        
CompanyLogoController.$inject= ['$scope','$http']
SuccessAlertController.$inject = ['$scope', 'sharedSuccessBoxService'];
MenuSectionController.$inject = ['$scope','$http', 'sharedSuccessBoxService','sharedSectionItemsService'];
SectionItemsController.$inject=['$scope','$http', 'sharedSuccessBoxService','sharedSectionItemsService'];
MenuDashboardController.$inject = ['$scope','$http'];
AddMenuItemsController.$inject = ['$scope','$http','Uploader'];
//AjaxUploadFileController.$inject = ['$scope','$upload']

//AjaxUploadFileController.$inject= ['$scope','$upload']



//on change, submit file for banner image
$(document.body).on('change','#bannerimageupload',function(){
	document.getElementById("menubannerimageform").submit()
})
//on change, submit file for company logo
$(document.body).on('change','#companylogoupload',function(){
	document.getElementById("menucompanylogoform").submit()
})
//on change, submit file for menuitem image
$(document.body).on('change','#addmenuitemimage',function(){
	document.getElementById("menuitemimageform").submit()
})



// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
